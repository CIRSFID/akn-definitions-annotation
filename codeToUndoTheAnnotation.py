import xml.etree.ElementTree as ET
from pprint import pprint

import os

# Specify the directory path
folder_path = 'files/input/LEOS/'

# List all files in the directory
file_names = os.listdir(folder_path)

# Print the file names
for file_name in file_names:
    # Load the XML file
    xml_file_path = f"files/input/LEOS/{file_name}"
# if True:
#     xml_file_path = f"files/input/LEOS/32015R0429.akn"
    xml_tree = ET.parse(xml_file_path)
    xml_root = xml_tree.getroot()

    # Define the namespace
    namespace = {"akn": "http://docs.oasis-open.org/legaldocml/ns/akn/3.0"}

    # Find elements using XPath with namespace prefixes
    element0 = xml_root.find('.//akn:definitions[@source="#unibo"]', namespace)
    element1 = xml_root.findall('.//akn:TLCTerm', namespace)
    element2 = xml_root.findall('.//akn:def', namespace)
    element3 = xml_root.findall('.//akn:defBody', namespace)

    for elem in xml_root.iter():
        for child in list(elem):
            if elem.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}definitions' and elem.attrib.get(
                    'source') == '#unibo':
                elem.remove(child)
                print("Removed element0 and its children")

    # Delete element1 and its children along with the tag itself
    for elem in xml_root.iter():
        for child in list(elem):
            if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}TLCTerm':
                elem.remove(child)

    # Delete element2 and its children along with the tag itself
    # for elem in xml_root.iter():
    #     for child in list(elem):
    #         if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}def':
    #             elem.remove(child)
    #             print("Found!")
    # Delete element3 and its children along with the tag itself
    # for elem in xml_root.iter():
    #     for child in list(elem):
    #         if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}defBody':
    #             elem.remove(child)

    # ele = xml_root.find('.//akn:definitions[@source="#unibo"]', namespace)
    # ele.attrib.pop('source')

    for elem in xml_root.iter():
        out_text = ""
        for child in list(elem):

            if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}definitions':
                if 'Source' in child.attrib:
                    elem.remove(child)
                    continue

            if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}point':
                if 'defines' in child.attrib:
                    del child.attrib['defines']
            if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}def' or child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}defBody':
                # elem.remove(child)
                if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}def':
                    if child.text is not None:
                        out_text = out_text + "\"" + child.text + "\" "
                        elem.remove(child)
                if child.text is not None:
                    out_text = out_text + child.text

                if child.tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}defBody':
                    print(f"{out_text}")

                    elem.remove(child)
                    elem.text = out_text

    # Save the modified XML tree back to a file
    for elem in xml_root.iter():
        if '}' in elem.tag:
            elem.tag = elem.tag.split('}', 1)[1]  # remove namespace prefix
    xml_tree.write(f"files/output/LEOS/{file_name}", encoding="utf-8", xml_declaration=True)
    # xml_tree.write(f"files/output/32015R0429.akn", encoding="utf-8", xml_declaration=True)

  # Use this file to find text patterns (using regex) quickly...
import os
import re
import sys
from pathlib import Path
import xml.etree.ElementTree as ET
from lxml import etree, objectify
import xml.dom.minidom as md

AKN_NAMESPACE = "http://docs.oasis-open.org/legaldocml/ns/akn/3.0"
FMX4_NAMESPACE = "http://formex.publications.europa.eu/schema/formex-05.56-20160701.xd"
XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance"
AKN_PREFIX = "{"+AKN_NAMESPACE+"}"
FMX_PREFIX = "{"+FMX4_NAMESPACE+"}"
AKN_PREFIX_XPATH = {"akomaNtoso": AKN_NAMESPACE}


def get_text_from_closest_empty_parent(el):
    # Getting the container (i.e. the closest container having no text nor tail!)
    container_el = el
    while container_el.getparent() is not None and (str(container_el.getparent().text).strip() not in ["None",""] or str(container_el.getparent().tail).strip() not in ["None",""]):
      container_el = container_el.getparent()
    container_text = u" ".join(container_el.itertext()).encode('ascii', 'ignore')
    return container_text

all_akn_files = list(Path("/content/data").rglob("*.[aKxX][kKmM][nNlL]"))
len(all_akn_files)

import xml.dom.minidom
import re
import html

for akn_file in all_akn_files:

  print(akn_file)
  # just read .akn and .xml
  if not (str(akn_file).endswith(".akn") or str(akn_file).endswith(".xml")):
      continue
  c=0
  num_value_new=0
  defs=[]
  refs=[]
  ls = []
  newls=[]
  subrefs=[]
  #print(f"\n\n===========\nThis file name is: {str(akn_file)}")

  #print(f"Reading derogations in {str(akn_file)}")
  with open(str(akn_file),"r", encoding="utf-8") as f:

      # we will print only if we found something
      output_found = False

      #Reading the content of the single akn file
      file_content = f.read()
      #print(file_content)

      the_file_path = str(akn_file)

      PATTERN_TO_FIND = r"(\'|\")\s(d|D)efinition(?!.*\b)"
      #PATTERN_TO_FIND = r"(\'|\")\s(d|D)efinition"
      PATTERN_TO_FIND_INNER = r"(\'|\"|\“|\”|\‘|\’|\„|\”|\«|\»)\s{1,}means" # '"“”‘’„”«»

      # turn file into an iterable xml
      akn_as_xml = etree.fromstring(ET.tostring(ET.fromstring(file_content), encoding='utf8', method='xml'))

      # Loop all the elements of the xml
      for el in akn_as_xml.iter():

          if not el.tag.endswith(AKN_PREFIX+"heading"):
            continue


          #print(f"\n\nThe text of this element is: {el.text}")
          #print(f"The tail of this element is: {el.tail}")
          if el.text != None and re.search(PATTERN_TO_FIND, el.text) or el.tail != None and re.search(PATTERN_TO_FIND, el.tail):
            #print(f"\n\n{the_file_path.split('/')[-1]} ::")
            #print(get_text_from_closest_empty_parent(el))
            pass

          if el.text and el.text.lower().find("definition") != -1:
            print("\n")
            #print(f"tag name: {el.tag.split('}')[-1]}")
            #print(f"text name: {el.text}")
            #print(f"tail name: {el.tail}")


            if el.getnext() is not None:
              this_sibling = el.getnext()
              print(f"/tMy next sibling is: {this_sibling.tag}")
              for x in this_sibling.iter():
                if x.text: # probably you don't need the tail, but doublecheck
                  if re.search(PATTERN_TO_FIND_INNER, x.text):
                    first_parent=x.getparent()
                    second_parent = first_parent.getparent()

                    try:
                      print(f"First parent:{first_parent}")
                      print(f"second parent:{second_parent.attrib['eId']}")

                      subls = []
                      #subls.append(second_parent.attrib['eId'])

                      #looking for multiples child of point tags
                      #for child in second_parent.findall(".//{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}point"):
                        #print(child.attrib)
                        #subls.append(child.attrib['eId'])

                      #ls.append(subls)
                      #print(ls)
                      #subls=[]
                      errorlist=[]
                      #num_value = second_parent.find(AKN_PREFIX+"num")

                    #print(second_parent)
                      #print(num_value)
                    except:
                      print("eId Missing!")

                    num_value_new+=1
                    count = 0
                    for p_tag in second_parent.findall(".//{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}list//{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}p"):
                         # count +=1
                          # Extract text content from <p> tag
                          p_text = p_tag.text
                          if p_text is not None:
                              # Create a new XML element for the new tag (e.g., <defBody>)
                              subls.append("defBody_" + str(num_value_new) +"-"+str(count))

                   # ls.append(subls)
                    #print(f"Here is ls: {ls}")
                    #subls=[]
                    #print(num_value_new)

                    #print(f"{x.text}")

                    #print(f"parent of X element : {}")

                    # catch the quoted element
                    quotedtext = None
                    defbodytext = None
                    QUOTES = r"(\'|\"|\“|\”|\‘|\’|\„|\”|\«|\«|\,|\<|\>|\»)"
                    QUOTED_TEXT_RX = r"(?P<quotedtext>"+QUOTES+r"([A-Za-z0-9-\s]{1,})"+QUOTES+")"
                    if re.search(QUOTED_TEXT_RX, x.text):
                      quotedtext = re.search(QUOTED_TEXT_RX, x.text).group("quotedtext")
                      print(f"THE QUOTED TEXT IS: {quotedtext}")
                      #print(x.text)
                      print("\n")

                    # get the text of the defBody
                      DEFBODY_RX = QUOTES+r"([A-Za-z0-9\s]{1,})"+QUOTES+"(?P<defbodytext>.*)"
                      if re.search(DEFBODY_RX, x.text):

                        defbodytext = re.search(DEFBODY_RX, x.text).group("defbodytext")
                        #print(f"THE DEF BODY TEXT IS: {defbodytext}")
                        c+=1
                    # we create a new <def> element
                    if quotedtext:


                      def_el = etree.Element(AKN_PREFIX+"def", eId='def_'+ str(num_value_new))
                      #print(f"Def Element here! {def_el.text}")
                      def_el.text = quotedtext[1:-1]

                      print(def_el.text)
                      defs.append(def_el.text)
                      refs.append('#def_'+ str(num_value_new))
                      second_parent.set('defines', '#def_'+ str(num_value_new))


                      # New Portion
                      counter=0
                      for p_tag in second_parent.findall(".//{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}list//{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}p"):
                          # Process each <p> tag as needed
                          #print(f"Found <p> tag inside def_el: {ET.tostring(p_tag, encoding='utf-8').decode('utf-8')}")
                          print(p_tag.getparent())
                          if p_tag.getparent().tag == '{http://docs.oasis-open.org/legaldocml/ns/akn/3.0}authorialNote':
                            print("authorialNote Found!")
                            continue

                          counter +=1
                          # Extract text content from <p> tag
                          p_text = p_tag.text

                          if p_text is not None and len(p_text)>0:
                              print(f"P text: {p_text}")
                              subrefs.append((str(num_value_new), "#defBody_" + str(num_value_new) + "-" + str(counter)))
                              #defBody_
                              #subrefs.append()
                              # Create a new XML element for the new tag (e.g., <defBody>)
                              new_tag = etree.Element(AKN_PREFIX + "defBody", eId="defBody_" + str(num_value_new) +"-"+str(counter))
                              new_tag.text = p_text
                              p_tag.text = ""
                              p_tag.append(new_tag)


                    # we create a new <defBody> element
                    if defbodytext:
                      defbody_el= etree.Element(AKN_PREFIX+"defBody",eId= "defBody_" + str(num_value_new))
                      defbody_el.text = defbodytext
                    print(f"Def Body text {defbodytext}")
                    if quotedtext and defbodytext:

                      # activating the flag to print the output
                      output_found = True


                      #
                      # PRESERVING PREVIOUS CHILDREN
                      #

                      # creating a dictionary for the children that we will append within the defBody (many times it will just be empty)
                      list_of_defbody_children = []

                      # we loop the childent of x
                      for e in x:
                        #print(e)
                        # let's store this loop element
                        position_of_el = x.index(e)
                        list_of_defbody_children.append(e)

                        try:
                        # check if the tail has an INTERRUPTION (semicolon or colon)
                          if e.tail.find(".") != -1:
                            break
                        except:
                          pass
                        #elif e.tail.find(";") != -1:
                        # CREATIVE SOLUTIONS
                        #
                        # ....
                        # ....
                        #

                      # now that we succesfully get all the children of x, we can add them one by one within our defBody
                      for child_el in list_of_defbody_children:
                        defbody_el.append(child_el)



                      #
                      # keeping everything in the original container (i.e. the original x)
                      #
                      x.text = None
                      x.insert(0,def_el)             # inserting new_x at the position xposition
                      x.insert(1,defbody_el)             # inserting new_x at the position xposition

                      x.text = '"'
                      def_el.tail = '"'


      #print(c)
      #print(akn_file)
      path= str(akn_file)
      file_name = path.split("/")[-1]
      file_name=file_name.split(".")

      # For CamelCase
      defss=[]
      for word in defs:
        words = word.split(" ")
        words[1:] = [w.title() for w in words[1:]]
        defss.append("".join(words))
      ls = [sorted(inner_list) for inner_list in ls]

      # Adding DefinitionHead And Body After ActiveModification Tag
      for el in akn_as_xml.iter():
        if el.tag == AKN_PREFIX+"meta":
            analysistag = el.find(AKN_PREFIX + "analysis")
            if analysistag is not None:
                #activemodifications = analysistag.find(AKN_PREFIX + "activeModifications")
                    analysistag.tail = '\n'
                    def_el = etree.Element(AKN_PREFIX + "definitions", source="#unibo")
                    def_el.text = '\n  '
                    def_el.tail = '\n       '
                    for i in range(len(defs)):
                        grandchild_el = etree.SubElement(def_el, AKN_PREFIX + "definition")
                        grandchild_el.set("refersTo", "#" +defss[i])
                        grandchild_el.text = '\n    '
                        grandchild_el.tail = '\n'
                        sub_grandchild_el = etree.SubElement(grandchild_el, AKN_PREFIX + "definitionHead")
                        sub_grandchild_el.set("href", refs[i])
                        sub_grandchild_el.set("refersTo", "#" + defss[i])
                        sub_grandchild_el.tail = '\n'
                        #grandchild_el.text = '\n'
                        defbody=refs[i].split("_")

                        new_sub_grandchild_el = etree.SubElement(grandchild_el, AKN_PREFIX + "definitionBody")
                        new_sub_grandchild_el.set("href","#defBody_"+defbody[1])
                        new_sub_grandchild_el.tail = '\n'
                        print(f"Value od Defbody: {defbody[1]}")
                        newcount = 0
                        for x in range(len(subrefs)):
                          if int(defbody[1]) == int(subrefs[x][0]):
                            newcount+=1
                            new_sub_grandchild_el = etree.SubElement(grandchild_el, AKN_PREFIX + "definitionBody")
                            new_sub_grandchild_el.set("href",subrefs[x][1])
                            new_sub_grandchild_el.tail = '\n'







                    #analysistag.addnext(def_el)
                    #analysistag.tail='\n'
                    analysistag.append(def_el)







            else:
              print("Analysis not found!")
# Adding TCLterm inside Reference Tag
      for el in akn_as_xml.iter():
        if el.tag == AKN_PREFIX+"meta":
          referencestag = el.find(AKN_PREFIX + "references")
          num_tags = len(referencestag)
          print(num_tags)
          if referencestag is not None:
            for i in range(len(defs)):
              def_el = etree.Element(AKN_PREFIX + "TLCTerm", eId=defss[i],href="/akn/ontology/terms/eu/"+defss[i],showAs=defs[i])
              referencestag.insert(num_tags+i, def_el)
              def_el.tail = '\n'

      for el in akn_as_xml.iter():


        # get the text of the defBody
        try:
          if x.tag == "p":
              def_text = ""
              defbody_text = ""
              print("X.tag:")
              print(x.tag)
              # Loop through the child elements of the <p> tag
              for child in x.iter():
                  if child.tag == "def":
                      def_text = child.text.strip()
                  elif child.tag == "defBody":
                      defbody_text = child.text.strip()

              if def_text and defbody_text:
                  # Remove new lines between quotation marks
                  def_text = re.sub(r'"(\n\s*<def\s*[^>]*>[^<]*<\/def>\n\s*)"', r'"\1"', def_text)
                  defbody_text = re.sub(r'"(\n\s*<defBody\s*[^>]*>[^<]*<\/defBody>\n\s*)"', r'"\1"', defbody_text)
        except:
          pass

      # TO SAVE THE NEW OUTPUT if the output_found flag is true
      if output_found:

        OUT_DIR = "/content/OUTPUT"               # <- this is the directory where you want to save the new output

        # transform the xml into a string
        akn_string = etree.tostring(
                akn_as_xml,
                xml_declaration=True,
                encoding="utf-8"
            ).decode("utf-8")


        akn_string = akn_string.replace("xmlns:ns0","xmlns").replace("ns0:","").replace(":ns0",":akn").replace(":ns1",":fmx").replace("ns1:","fmx:").replace("&quot;",'"')


        akn_string = re.sub(r'\n(?=\s*")', '', akn_string)

        # make sure the output directory exist
        if not os.path.isdir(OUT_DIR):
          os.mkdir(OUT_DIR)
        dom = md.parseString(akn_string)
        pretty_xml = dom.toprettyxml()

        # Remove namespace prefix from output
        #pretty_xml = pretty_xml.replace('&quot;', '')
        #pretty_xml = pretty_xml.replace('ns0:', '')

        # remove the weird newline issue:
        pretty_xml = os.linesep.join([s for s in pretty_xml.splitlines()
                                      if s.strip()])


        decoded_xml_string = html.unescape(str(pretty_xml))
        # write the final output
        out_file_path = os.path.join(OUT_DIR, str(akn_file).split("/")[-1].split(".")[0] + "_modify_" + ".akn") # here you can chose the name of the output file
        with open(out_file_path, "w") as f:

          akn_string = decoded_xml_string.encode("utf-8")
          akn_string = '<?xml version="1.0" encoding="UTF-8"?>\n' + decoded_xml_string
          akn_string = akn_string.replace('<?xml version="1.0" ?>',"")
          f.write(akn_string)

#ouput folder

all_akn_files = list(Path("/content/OUTPUT").rglob("*.[aKxX][kKmM][nNlL]"))
all_akn_files